var app = angular.module('angular-vulnd', []);

app.filter("safe",["$sce", function($sce) {
    return function(text) {
        return $sce.trustAsHtml(text);
    };
}]);

app.filter("checkmark", function() {
    return function(input) {
        return input ? '\u2713' : '';
    };
});

app.controller("ObjCtrl", function($http, $scope) {
    
    $http.get("/static/localization/en.json").then(
        function(response) {
            $scope.localization = response.data;
        },
        function(error_response) {
            // Handle errors here
            alert("Error: Failed to get localization data");
        } 
    ); 

    $http.get("/api/objs").then(
        function(response) {
            $scope.objects = response.data.objects;
        },
        function(error_response) {
            // Handle errors here
            alert("Error: Failed to get objects data");
        } 
    ); 

    $scope.deleteObj = function(id){
        $http.delete("/api/objs/" + id).then(
            function(response) {
                $http.get("/api/objs").then(
                    function(response) {
                        $scope.objects = response.data.objects;
                    },
                    function(error_response) {
                        // Handle errors here
                        alert("Error: Failed to get updated objects data");
                    } 
                ); 
            },
            function(error_response) {
                // Handle errors here
                alert("Error: Failed to delete object");
            } 
        );
    };

    $scope.newObjSubmit = function (){
        $http.post("/api/objs", $scope.newobj).then(
            function(response) {
                // Handle success case here
                $http.get("/api/objs").then(
                    function(response) {
                        $scope.objects = response.data.objects;

                        $("#newObjModal").modal('hide');

                        document.forms["newObjForm"].reset();
                        document.getElementById("nameInput").value = "";
                        document.getElementById("intInput").value = "";
                        document.getElementById("boolInput").value = false;
                    },
                    function(error_response) {
                        // Handle errors here
                        alert("Error: Failed to get objects data");
                    } 
                ); 
            }, 
            function(error_response) {
                // Handle errors here
                alert("Error: Failed to create object");
            }
        );
    };

    $("#newObjSubmitButton").click(function (){
        $scope.newObjSubmit();
    });

});


