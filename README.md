# vulnd-json-xss

An app that showcases cross-site scripting vulnerabilities present in frontend view rendering frameworks.

![](https://i.imgur.com/xJfdyVz.png)

## Usage
`FLASK_APP=app.py FLASK_ENV=development flask run`