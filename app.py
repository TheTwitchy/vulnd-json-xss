import flask
from flask import Flask, session


import os
import json
import random
import uuid
import datetime
from functools import wraps


DB_FILE = "objects_db.json"


def check_user_id(f):
    @wraps(f)
    def decorated_func(*args, **kwargs):
        try:
            session["user_id"]
            return f(*args, **kwargs)
        except KeyError:
            user_id = str(uuid.uuid4().hex)
            session["user_id"] = user_id
            return f(*args, **kwargs)

    return decorated_func


app = Flask(__name__)
app.config.update(TEMPLATES_AUTO_RELOAD=True)
if os.environ.get("FLASK_ENV") == "dev":
    app.secret_key = "the_super_secret_key"
else:
    app.secret_key = os.urandom(32)


@app.route("/")
@check_user_id
def index():
    return flask.render_template("main.html")


@app.route("/angularjs")
@check_user_id
def angularapp():
    return flask.render_template("angular.html")


@app.route("/api/objs", methods=["GET"])
@check_user_id
def get_objs():
    db_filename = os.path.join(os.getcwd(), DB_FILE)
    if not os.path.isfile(db_filename):
        tmp_db = {}
        tmp_db["objects"] = []

        with open(db_filename, "w") as fd:
            fd.write(json.dumps(tmp_db))

    with open(db_filename, "r") as fd:
        db_str = fd.read()

    tmp_db = json.loads(db_str)
    tmp_list = []
    for obj in tmp_db["objects"]:
        if datetime.datetime.strptime(
            obj["created_at"], "%Y-%m-%dT%H:%M:%S.%f"
        ) < datetime.datetime.now() + datetime.timedelta(days=1):
            tmp_list.append(obj)

    # Write the new list back to the file, this should not have objects older
    # than 1 day. Technically there's also a bug where if there is an expired
    # object owned by the user then it will be shown once to the user, but I
    # don't care enough to fix it.
    db = {}
    db["objects"] = tmp_list
    with open(db_filename, "w") as fd:
        fd.write(json.dumps(db))

    ret_db = {}
    ret_db["objects"] = []
    for obj in tmp_list:
        if obj["owner"] == session["user_id"]:
            ret_db["objects"].append(obj)

    resp = flask.Response(json.dumps(ret_db))
    resp.mimetype = "application/json"
    return resp


@app.route("/api/objs", methods=["POST"])
@check_user_id
def new_obj():
    # this split is because sometimes the content type can have a charset
    # option after it.
    ct = flask.request.content_type.split(";")[0]
    if not ct == "application/json":
        flask.abort(400)

    db_filename = os.path.join(os.getcwd(), DB_FILE)

    if not os.path.isfile(db_filename):
        db = {}
        db["objects"] = []

    else:
        with open(db_filename, "r") as fd:
            db = json.loads(fd.read())

    try:
        req_dict = flask.request.get_json()

        new_obj = {}
        # Give it a random ID
        new_obj["id"] = random.randint(1, 10000000)

        new_obj["name"] = req_dict["name"]

        new_obj["owner"] = session["user_id"]
        new_obj["created_at"] = datetime.datetime.now().isoformat()

        try:
            new_obj["bool"] = req_dict["bool"]
        except KeyError:
            new_obj["bool"] = False

        new_obj["int"] = req_dict["int"]

    except Exception:
        flask.abort(400)

    db["objects"].append(new_obj)

    with open(db_filename, "w") as fd:
        fd.write(json.dumps(db))

    resp = flask.Response(status=201)
    return resp


@app.route("/api/objs/<int:obj_id>", methods=["DELETE"])
@check_user_id
def del_obj(obj_id):
    db_filename = os.path.join(os.getcwd(), DB_FILE)

    if not os.path.isfile(db_filename):
        resp = flask.Response(status=204)
        return resp

    else:
        with open(db_filename, "r") as fd:
            db = json.loads(fd.read())

    for obj in db["objects"]:
        if obj["id"] == obj_id:
            db["objects"].remove(obj)

    with open(db_filename, "w") as fd:
        fd.write(json.dumps(db))

    resp = flask.Response(status=204)
    return resp


if __name__ == "__main__":
    app.run()
